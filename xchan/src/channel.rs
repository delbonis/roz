#![allow(unused)]

use std::collections::{HashMap, HashSet};
use std::hash::{Hash, Hasher};

use core::addr::LnAddr;
use core::blob::*;
use core::coin::*;
use core::io::{self, BigEndian, BinaryRep, ReadBytesExt, WriteBytesExt};
use core::sig;

#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub struct ChannelParticipant {
    opsig_pubkey: sig::Pubkey,
    chain_pubkey: ChainPubkey,
}

#[derive(Clone, Debug)]
pub struct ChannelContext {
    coin: CoinId,
    participants: HashMap<LnAddr, ChannelParticipant>,
}

impl ChannelContext {
    pub fn has_participant(&self, addr: &LnAddr) -> bool {
        self.participants.contains_key(addr)
    }
}

#[derive(Clone, Debug)]
pub struct ChannelState {
    // These members should not be changed by operations. (!!!)
    inputs: Vec<TxIn>,

    // These members can be changed by operations.
    parts: HashMap<String, Partition>,
    dependent_parts: Vec<String>, // names of partitions that produce subchannels
}

impl ChannelState {
    pub fn new_with_parts(self, parts: HashMap<String, Partition>) -> ChannelState {
        ChannelState { parts, ..self }
    }

    pub fn partition(&self, name: &String) -> Option<&Partition> {
        self.parts.get(name)
    }

    pub fn parts(&self) -> &HashMap<String, Partition> {
        &self.parts
    }

    pub fn inputs(&self) -> &Vec<TxIn> {
        &self.inputs
    }
}

#[derive(Clone, Debug)]
pub struct Partition {
    funds: u64,
    typedata: PartitionTypeData,
}

#[derive(Clone, Debug)]
pub enum PartitionTypeData {
    UserBalance {
        // ID = 0x00
        drain: ChainPubkey,
    },
    Htlc {
        // ID 0x01
        dest: ChainPubkey,
        refund: ChainPubkey,
        timeout: u32, // this has multiple interpretations, see nLockTime
        prehash: [u8; 32],
    },
    Subchannel {
        // ID 0x02
        // should we define how the funds are distributed?  or should we encode that logic at a higher level?
        members: HashSet<LnAddr>,
    },
}

impl Partition {
    pub fn balance(&self) -> u64 {
        self.funds
    }
    pub fn typedata(&self) -> &PartitionTypeData {
        &self.typedata
    }
    pub fn to_new_delta(&self, funds_diff: i64) -> Partition {
        Partition {
            funds: (self.funds as i64 + funds_diff) as u64, // TODO Check for overflows.
            typedata: self.typedata.clone(),
        }
    }
    pub fn to_contract(&self, ctx: &ChannelContext) -> Contract {
        unimplemented!()
    }
}

impl Hash for Partition {
    fn hash<H: Hasher>(&self, h: &mut H) {
        // Serialize ourselves to a temp buffer.
        let mut buf = Vec::new();
        self.serialize(&mut buf);

        // Then just hash all the bytes in serialized representation.
        for b in buf {
            h.write_u8(b);
        }
    }
}

include!("channel_io.rs");
