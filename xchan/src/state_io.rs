impl io::BinaryRep for StateId {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> io::Result<Self> {
        let mut buf = [0; 32];
        reader.read_exact(&mut buf)?;
        Ok(StateId(buf))
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> io::Result<()> {
        Ok(writer.write_all(&self.0)?)
    }
}

impl io::BinaryRep for StateNode {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> io::Result<Self> {
        use self::StateNode::*;
        match reader.read_u8()? {
            0x00 => {
                // This we literally just dump the channel state.
                let chan = ChannelState::deserialize(reader)?;
                Ok(Root { init: chan })
            }
            0x01 => {
                // Parse the parent StateId.
                let par = StateId::deserialize(reader)?;

                // And the transition, which is applied the the previous state.
                let tsn = StateTransition::deserialize(reader)?;

                // Construct it.
                Ok(Deriv {
                    parent: par,
                    transition: tsn,
                })
            }
            0x02 => {
                // Parse the parent StateId.
                let par = StateId::deserialize(reader)?;

                // Parse the new host StateId.
                let host = StateId::deserialize(reader)?;

                // Construct it.
                Ok(Reanchor {
                    parent: par,
                    host: host,
                })
            }
            n @ _ => Err(io::BinaryError::new_unexpected(format!(
                "got unknown discrim of {}",
                n
            ))),
        }
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> io::Result<()> {
        use self::StateNode::*;
        match self {
            Root { init } => {
                // Write discrim.
                writer.write_u8(0x00)?;

                // Write channel state.
                init.serialize(writer)?;
            }
            Deriv { parent, transition } => {
                // Write discrim.
                writer.write_u8(0x01)?;

                // Write parent state ID.
                parent.serialize(writer)?;

                // Write the transition.
                transition.serialize(writer)?;
            }
            Reanchor { parent, host } => {
                // Write discrim.
                writer.write_u8(0x02)?;

                // Write parent state ID.
                parent.serialize(writer)?;

                // Write the new host state.
                host.serialize(writer)?;
            }
        }
        Ok(())
    }
}
