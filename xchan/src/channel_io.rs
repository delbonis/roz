impl io::BinaryRep for ChannelState {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> io::Result<Self> {
        unimplemented!() // TODO Finish this.
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> io::Result<()> {
        unimplemented!() // TODO Finish this.
    }
}

impl io::BinaryRep for Partition {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> io::Result<Self> {
        use self::PartitionTypeData::*;

        // Reading the funds is simple.
        let funds = reader.read_u64::<BigEndian>()?;

        // Pulling out the actual partition data is a little more complicated.
        let data = match reader.read_u8()? {
            0x00 => {
                // This one we just read a buf for the pubkey, hope it fits.
                let d = SmallBlob::deserialize(reader)?;
                UserBalance {
                    drain: ChainPubkey::from_vec(d.into_inner()),
                }
            }
            0x01 => {
                // Read the dest address, again, hope it fits.
                let d = SmallBlob::deserialize(reader)?;

                // Read the refund address
                let r = SmallBlob::deserialize(reader)?;

                // Read the timeout, always BE.
                let to = reader.read_u32::<BigEndian>()?;

                // Reading the preimage hash is a little awkward.
                let mut ph = [0; 32];
                reader.read_exact(&mut ph)?;

                // Now actually construct everything.
                Htlc {
                    dest: ChainPubkey::from_vec(d.into_inner()),
                    refund: ChainPubkey::from_vec(r.into_inner()),
                    timeout: to,
                    prehash: ph,
                }
            }
            0x02 => {
                // TODO Figure this out later
                unimplemented!()
            }
            n @ _ => {
                return Err(io::BinaryError::new_unexpected(format!(
                    "unknown partition discrim {}",
                    n
                )))
            }
        };
        Ok(Partition {
            funds: funds,
            typedata: data,
        })
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> io::Result<()> {
        use self::PartitionTypeData::*;
        writer.write_u64::<BigEndian>(self.funds)?;
        match self.typedata {
            UserBalance { ref drain } => {
                // Write discrim.
                writer.write_u8(0x00)?;

                // Hope that the pubkey fits, if not then oops!
                let d = SmallBlob::from_slice_unchecked(drain.as_slice());
                d.serialize(writer)?;
            }
            Htlc {
                ref dest,
                ref refund,
                timeout,
                prehash,
            } => {
                // Write discim.
                writer.write_u8(0x01)?;

                // Write dest pubkey.
                SmallBlob::from_slice_unchecked(dest.as_slice()).serialize(writer)?;

                // Write refund pubkey.
                SmallBlob::from_slice_unchecked(refund.as_slice()).serialize(writer)?;

                // Write timeout, BE.
                writer.write_u32::<BigEndian>(timeout)?;

                // Write preimage hash.
                writer.write_all(&prehash)?;
            }
            Subchannel { ref members } => {
                // TODO Figure this out later
                unimplemented!();
            }
        }
        Ok(())
    }
}
