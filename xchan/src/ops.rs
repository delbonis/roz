#![allow(unused)]

use std::collections::*;
use std::hash::{Hash, Hasher};

use core::addr::LnAddr;
use core::io::{self, BigEndian, ReadBytesExt, WriteBytesExt};
use core::merkle;
use core::sig::Signature;

use channel::*;

/// Container type for manipulating the layout of partitions in a channel during
/// a state transition.
#[derive(Clone, Hash, Debug)]
pub struct Op {
    /// The latest time that this Op can be included in a state transition,
    /// compared against the transition's time.  It also affords some very
    /// basic but effective replay protection.
    ///
    /// Set to 0xffffffffffffffff to disable timestamp checking.
    ttl_unix: u64,

    /// The actual data to perform this operation.
    typedata: OpTypeData,
}

/// Logical behavior for a transition operation.
#[derive(Clone, Hash, Debug)]
pub enum OpTypeData {
    /// Simple push operation to increase the drain balance for a member of a
    /// channel, draining funds from the single signer's balance.
    Push {
        // ID 0x01
        amt: u64,
        dest: LnAddr,
    },
    /// Takes balance from the single signer, and creates and HTLC with the
    /// specified parameters, going to the specified user or draining back to
    /// the original sender.
    AddHtlc {
        // ID 0x10
        size: u64,
        timeout: u32,
        prehash: [u8; 32],
        dest: LnAddr,
        name: String,
    },
    /// Claims an HTLC with the preimage and sending it back to the dest user.
    ClaimHtlc {
        // ID 0x11
        preimage: Vec<u8>,
        name: String,
    },
    /// Reclaims an expired HTLC back to the user that created it.
    ExpireHtlc {
        // ID 0x12
        name: String,
    },
    /// Creates a subchannel, draining balance from the signers according to the
    /// amounts specified.
    FundSubchannel {
        // ID 0x20
        name: String,
        /// Merkle root of LN addresses.
        member_commitment: merkle::MerkleRoot,
        /// The indexes here are the indexes of the op sigs in the container.
        funder_withdrawals: Vec<u64>, // should be alphabetical by LnAddr
    },
    /// Closes a subchannel with the final state layout provided.
    CloseSubchannel {
        // ID 0x21
        name: String,
        /// Layout of the partitions from the state we're closing into the parent.
        final_state_cfg: Vec<Partition>, // should be alphabetical by name
    },
}

/// Side effects that operations can have on the local environment, right now
/// it's just create/destroy subchannels, but there might be more later.
///
/// I'm not sure if this is the best place for these since we have to have some
/// extra check that we're actually in the subchannel that got created, which
/// seems bad because it's asymmetric.  Maybe also use this with HTLC expiry?
#[derive(Clone, Debug)]
pub enum OpSideEffect {
    CreateSubchannel(ChannelState, String),
    DestroySubchannel(String),
}

pub enum TransitionError {
    InsuffientFunds,
    UnknownParticipant,
    UnknownPartitionFormat,
    OpSigFail,
    BalanceOverflow,
    Other(String),
}

fn apply_op(
    ctx: &ChannelContext,
    state: ChannelState,
    aop: &AuthedOp,
) -> Result<(ChannelState, Vec<OpSideEffect>), TransitionError> {
    use self::OpTypeData::*;
    use self::TransitionError::*;

    // Verify the sigs are participants in the channel.
    for (a, _) in &aop.sig_pairs {
        if !ctx.has_participant(&a) {
            return Err(UnknownParticipant);
        }
    }

    // Extract the actual operation now that we've verified sigs
    let op = &aop.op;

    // Find their own channel, we're probably making modifications to it.
    /*let sender_bal = match state.partition(&op.sender.as_string()) {
        Some(p) => p.balance(),
        None => return Err(UnknownPartitionFormat),
    };*/

    match &op.typedata {
        &Push { amt, ref dest } => {
            // Verify sig count valid.
            if aop.sig_pairs.len() != 1 {
                return Err(OpSigFail);
            }

            // Find the sender's balance.
            let sender_bal = match state.partition(&aop.sig_pairs[0].0.as_string()) {
                Some(p) => p.balance(),
                None => return Err(UnknownPartitionFormat),
            };

            // Make sure the sender's balance is good.
            if amt > sender_bal {
                return Err(InsuffientFunds);
            }

            // Figure out the new partitions for the state.
            let new_parts = state
                .parts()
                .iter()
                .map(|(n, p)| (n.clone(), p.clone()))
                .map(|(n, p)| match n.clone() {
                    // TODO Verify no overflow.
                    sender => (n, p.to_new_delta(amt as i64 * -1)),
                    dest => (n, p.to_new_delta(amt as i64)),
                    _ => (n, p),
                })
                .collect();

            Ok((state.new_with_parts(new_parts), vec![]))
        }

        AddHtlc {
            size,
            timeout,
            prehash,
            dest,
            name,
        } => unimplemented!(),

        ClaimHtlc { preimage, name } => unimplemented!(),

        ExpireHtlc { name } => unimplemented!(),

        _ => Err(Other("not yet implemented".into())),
    }
}

#[derive(Clone, Debug)]
pub struct AuthedOp {
    op: Op,
    sig_pairs: Vec<(LnAddr, Signature)>,
}

impl Hash for AuthedOp {
    fn hash<H: Hasher>(&self, h: &mut H) {
        use core::io::BinaryRep;
        let mut buf = Vec::new();
        self.serialize(&mut buf);
        h.write(buf.as_ref());
    }
}

#[derive(Clone, Hash, Debug)]
pub struct StateTransition {
    timestamp: u64,
    ops: Vec<AuthedOp>,
}

pub fn apply_transition(
    ctx: &ChannelContext,
    state: ChannelState,
    tsn: &StateTransition,
) -> Result<(ChannelState, Vec<OpSideEffect>), TransitionError> {
    // Just iterate through all the operations in the state, early-returning in
    // case of errors.
    let mut cur = state;

    let mut side_effects = vec![];

    for op in &tsn.ops {
        // Calculate the total balance of the partitions.
        let before_bal: u64 = cur.parts().values().map(|p| p.balance()).sum();

        // Apply the operation to the channel.
        let (c, sides) = apply_op(ctx, cur, &op)?;
        cur = c;
        side_effects.extend(sides);

        // Now check to see if we overflowed the balance.
        // TODO Make this check more robust, what about fees?
        let after_bal: u64 = cur.parts().values().map(|p| p.balance()).sum();
        if after_bal > before_bal {
            return Err(TransitionError::InsuffientFunds);
        }
    }

    // If we got here then it should be all ok.
    Ok((cur, side_effects))
}

include!("ops_io.rs");
