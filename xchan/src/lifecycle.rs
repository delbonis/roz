#![allow(unused)]

use std::collections::*;

use core::coin::*;

use crate::ops;
use crate::state::{self, StateId};

pub type ChannelId = String; // TODO

#[derive(Clone, Hash, Debug)]
enum ChannelFunder {
    Chan(ChannelId),
    ChainTx(Txid),
}

#[derive(Debug)]
pub struct ChannelEnv {
    coin: CoinId,
    roots: Vec<ChannelId>,
    funding_tree: HashMap<ChannelId, ChannelFunder>,
    state_trackers: HashMap<ChannelId, StateTracker>,
}

#[derive(Hash, Debug)]
struct StateTracker {
    /// States for which there are no signatures.
    unsigned: Vec<StateId>,

    /// States that we have published signatures for but have not received all
    /// other signatures for, so cannot publish yet.
    signing: Vec<StateId>,

    /// States that are valid to publish right now, and we have all the sigs to
    /// do that.
    signed: Vec<StateId>,

    /// States that we have revoked but not everyone else has produced
    /// revocation signatures for.  No longer safe to broadcast.
    revoking: Vec<StateId>,
}

/// Handles the auxiliary state about a channel, like which txids it should care
/// about when processing blocks.  This should be given to the overall manager
/// so it knows when to do what.  Updated by certain side effects, not directly.
#[derive(Debug)]
pub struct ChannelController {
    /// Which txids the channel should be notified about when events happen.
    watch_txids: Vec<Txid>,

    /// Delayed inputs and when they should be given back to the state machine.
    delayed_inputs: HashMap<String, (u64, ExternalInput)>,

    /// If we should end the channel by a certain time, like in Eltoo, then we
    /// should re-sign the tx by this time.  This is the final deadline, so if
    /// the current time is after this time, we're kinda in trouble.
    ///
    /// So we should have some kind of thing for deciding when to regularly sign
    /// new states in this kind of system.
    collapse_deadline: Option<u64>,

    /// If this is not-None, then it means the channel has failed and there are
    /// some user messages about that.
    failed_messages: Option<Vec<String>>,

    /// Has the channel halted?  This should be false most of the time.
    halted: bool,
}

type StateCloseSig = (); // TODO

/// Some kind of external input to a channel.  This includes things coming in
/// from the blockchain, inputs from the other peer, and inputs from the user.
///
/// Having the inputs be a single known finite set of items lets us keep very
/// accurate logs of what the state machine is doing and then play it back in
/// the original order to see how the state evolves over time and what it tries
/// to do.  This makes debugging things so much simpler.
#[derive(Clone, Debug)]
pub enum ExternalInput {
    /// When a tx (that we care about) has entered the mempool.
    TxBroadcast(Txid),

    /// When a tx (that we care about) has been confirmed in a block.
    TxConfirm(Txid),

    /// When the block this tx was in has some number of blocks on top of it and
    /// should be deemed as "safe".
    TxMature(Txid),

    /// When we've received a sig from a peer for this channel.
    RecvPeerSig(StateId, state::StateSig),

    /// When we've received a rev from a peer for this channel.  Not used in Eltoo.
    RecvPeerRev(StateId, state::StateRev),

    /// When we're decided on a transition we want to perform on top of the
    /// state tips.  This might be coming directly from the user or it might be
    /// from whatever's handling the op mempool for this channel.
    ExecSubmitTransition(ops::StateTransition),

    /// When we've decided we want to do a coopeative close at a given state.
    /// Will reject if it's not one of the valid unrevoked states.
    ExecCloseChannel(StateId),

    /// When we've decided we want to break at a given state.  Will reject if
    /// it's not one of the valid unrevoked states.
    ExecBreakChannel(StateId),
}

/// Some side effect that the state machine will produce after given an input.
#[derive(Clone, Debug)]
pub enum ExternalSideEffect {
    /// A new state node has been created, put it in the database and stuff.
    StateCreated(state::StateNode),

    /// Mark a state ID as having been accepted, but not finalized (yet).  This
    /// is used for tracking op confirmation.
    AcceptState(StateId),

    /// Mark a state ID as having been finalized as "the current state".  This
    /// is used for tracking op confirmation.
    FinalizeState(StateId),

    /// Mark a state ID as us having revoked it, not to be broadcast.
    RevokeState(StateId),

    /// Mark a state ID as having been destroyed, so that nobody should be
    /// broadcasting it.
    InvalidateState(StateId),

    /// Actually delete the state from the DB.  Used when reorganizing state
    /// history and other stuff I gues?
    DeleteState(StateId),

    /// Broadcasts a tx to the network now.
    BroadcastTx(Box<[u8]>),

    /// Mark a tx as "notify me if something happens with this".
    WatchTx(Txid),

    /// Mark a tx as "don't notify me if something happens with this anymore".
    IgnoreTx(Txid),

    /// Give this input back to this channel after a certain (UNIX) time.  For
    /// like HTLC expiry and timeouts in case state updates fail and we should
    /// break the channel or try again later.
    ///
    /// (input, unix, label)
    AddDelayedInput(ExternalInput, u64, String),

    /// Cancel the delayed input, like if the HTLC was claimed with preimage.
    CancelDelayedInput(String),

    /// Unix time for when we *must* resign the state or we won't necessarily be
    /// able to enforce absolute timelocks on-chain.  This is only used in
    /// Eltoo-like systems.  (Thanks ZmnSCPxj!)
    ///
    /// This would be provided by the update mechanism implementation.
    SetResignDeadline(u64),

    /// Cancel resign deadline, not sure why we'd ever need to use this.  Maybe
    /// when we halt we should also emit this?
    ClearResignDeadline,

    /// This channel isn't interesting anymore, you can clean it up now.  This
    /// would probably be done after a TxMature input or something.
    Halt,

    /// Something weird happened, lock it up and wait for manual intervention?
    Fail(String),
}
