#![allow(unused)]

use core::coin::*;
use core::crypto::digest::Digest;
use core::crypto::sha2;
use core::io::{self, BigEndian, BinaryRep, ReadBytesExt, WriteBytesExt};
use core::sig::*;

use channel::*;
use ops::*;

use std::collections::*;
use std::fmt;

pub enum StateError {
    InvalidRoot,
    InvalidNodeLayout,
    TsnFail(TransitionError),
    SigFail(String),
    NotFound(StateId),
    Other(String),
}

impl From<TransitionError> for StateError {
    fn from(v: TransitionError) -> Self {
        StateError::TsnFail(v)
    }
}

type Result<T> = ::std::result::Result<T, StateError>;

/// State IDs are the hashes of the serialized state block.
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct StateId([u8; 32]); // just a hash

impl fmt::Display for StateId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // This just prints the chars in order all together.
        for b in self.0.iter() {
            write!(f, "{:02x?}", b);
        }
        Ok(())
    }
}

impl fmt::Debug for StateId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        <StateId as fmt::Display>::fmt(self, f) // Just call the impl for Display, it's the same.
    }
}

#[derive(Clone, Debug)]
pub enum StateNode {
    Root {
        // ID = 0x00
        init: ChannelState,
    },
    Deriv {
        // ID = 0x01
        parent: StateId,
        transition: StateTransition,
    },
    Reanchor {
        // ID = 0x02
        parent: StateId,
        host: StateId,
    },
}

impl StateNode {
    /// Computes the flat content-addressed StateId of this state.
    pub fn to_state_id(&self) -> StateId {
        // Serialize the state.
        let mut buf = Vec::new();
        self.serialize(&mut buf).unwrap();

        // Hash it.
        let mut h = sha2::Sha256::new();
        h.input(buf.as_ref());
        let mut hash = [0; 32];
        h.result(&mut hash);

        // And return the hash as a StateId.
        StateId(hash)
    }
    /// Returns the primary parent to this state, if it has one.  Recursively
    /// calling this function should eventually lead to a Root node, which would
    /// return None for this function.
    pub fn get_parent(&self) -> Option<StateId> {
        use self::StateNode::*;
        match self {
            Deriv { parent, transition } => Some(*parent),
            Reanchor { parent, host } => Some(*parent),
            _ => None,
        }
    }
}

/// Represents something that can get state nodes.
pub trait StateNodeSource {
    /// Gets the state node with the provided ID if it's there.
    fn get_node(&self, id: &StateId) -> Result<StateNode>;
}

/// Traces the state history back to Roots to compute the real ChannelState of
/// the node specified, requesting node data from the source provided.
fn compute_state<S: StateNodeSource>(
    src: &S,
    ctx: &ChannelContext,
    node_id: &StateId,
) -> Result<ChannelState> {
    use self::StateError::*;
    use self::StateNode::*;

    // First we need to compute all the states we need to traverse, this is just
    // walking up the list pushing onto a stack.
    let mut cur = src.get_node(node_id)?;
    let mut eval_stack = Vec::new();
    eval_stack.push(cur.clone());
    loop {
        let prev = match cur.get_parent() {
            Some(psid) => src.get_node(&psid)?,
            None => break,
        };
        eval_stack.push(prev.clone());
        cur = prev;
    }

    // Now we figure out the initial state, usually just a Root.
    let mut cur_state = match eval_stack.pop().unwrap() {
        Root { init } => init,
        _ => return Err(InvalidRoot),
    };

    // Now we work down the stack applying the ops to each state.
    while let Some(next) = eval_stack.pop() {
        match next {
            Deriv { parent, transition } => {
                // Just apply the transition, continue.  (Ignore side effects?)
                let (after, _) = apply_transition(ctx, cur_state, &transition)?;
                cur_state = after;
            }
            Reanchor { parent, host } => {
                // Recurse upwards, get the state we're building off of.
                let parent_state = compute_state(src, ctx, &parent)?;

                // TODO Copy the outputs from the parent state to our own state's outputs.
                unimplemented!()
            }
            _ => return Err(InvalidNodeLayout),
        }
    }

    // Now just return the final state.
    Ok(cur_state)
}

pub type StateSig = (Pubkey, Signature); // TODO
pub type StateRev = (Pubkey, Signature); // TODO

#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub struct PendingSig {
    particpant: ChannelParticipant,
}

pub type PendingRev = PendingSig;

/// State machine for tracking update info for state transitions, this is for
/// the traditional LN commitment scheme, doesn't work like this with eltoo, but
/// it's not too far off, in fact we can actually just skip the waiting for revs
/// part since eltoo doesn't use those.
///
/// Unfortunately this actually would get really really messy for doing
/// multiparty channels with LN-penalty since you get exponential blowup. The
/// real way to do that would be with a pushdown automaton since we can't just
/// do all of the txs in the revocation tree in parallel, lower txs in the tree
/// have hard dependencies on higher ones, but maybe there's some weird thing we
/// could do with SIGHASH_NOINPUT and some decreasing relative timelocks to
/// avoid having to sign every single tx individually?  Look into this some
/// other time.
#[derive(Clone, Debug)]
pub enum UpdateState {
    /// Nothing particularly interesting is happening.
    Idle,

    /// We've decided on a next state and are negotiating sigs for it.
    UpdateAwaitSigs {
        next: ChannelState,
        needed: HashSet<PendingSig>,
        sigs: HashSet<StateSig>,
    },

    /// Wait until all of the children have re-signed for us now.
    UpdateReanchorChildren {
        waiting_txos: HashSet<Txid>, // TODO
        exit_publish: Vec<UpdateSideEffect>,
        next_mach_state: Box<UpdateState>,
    },

    /// We're gotten all the sigs and now we're just waiting for revs.
    UpdateAwaitRevs {
        next: ChannelState,
        needed: HashSet<PendingRev>,
        revs: HashSet<StateRev>,
    },
}

/// These side effects are actions to take after a state transition to effect
/// things outside our update state, like stuff with sigs/revs or to trigger
/// other channels to update.
#[derive(Clone, Debug)]
pub enum UpdateSideEffect {
    /// Gossip this sig to the relevant parties.
    BroadcastSig(StateSig),

    /// Gossip this rev to the relevant parties.
    BroadcastRev(StateRev),

    /// Publish this raw tx data to the host chain.
    PublishTx(Vec<u8>),

    /// Record these sigs in the DB, somehow.
    RecordSigs(HashSet<StateSig>),

    /// Record these revs in the DB in case we need them.
    RecordRevs(HashSet<StateRev>),

    /// Mark the channel as "need break" and break it when appropriate.
    BreakChannel,

    /// Update the "known state" outside the machine (on this branch?), also
    /// signals to waiters that the state update has completed.
    CommitState(ChannelState),

    /// Here we should wait until we have sigs for txs spending from all of
    /// these txids before actually moving onto the next state outwardly.
    ReanchorSubchannel(HashSet<Txid>), // TODO Figure out these details.

    /// Says "this input wasn't actually processed, give it to me later", which
    /// will just add the input to a vec and keep it around somewhere.
    DeferInput(UpdateInput),

    /// Says "I'm ready to process some of the things that were deferred", and
    /// then it should immediately have the deferred inputs be send to it.
    ProcessDeferred,

    /// We've finished performing the update and we can cleanup this machine.
    Finish,
}

#[derive(Clone, Debug)]
pub enum UpdateInput {
    /// We've received a sig from a remote peer.
    RecvSig(StateSig),

    /// We've received a rev from a remote peer.
    RecvRev(StateRev),

    /// A subchannel has committed to a state.
    ///
    /// See UpdateSideEffect::CommitState
    SubchannelCommitment,

    /// A superchannel has updated and we need to re-sign our state.
    SuperchannelUpdate(HashSet<TxIn>), // TODO Figure this out.
}

#[derive(Clone, Debug)]
pub struct UpdateContext {
    /// This is the actual important state machine bit.  I'm not sure if this
    /// should actually be here or not.
    update_state: UpdateState,

    /// Vec of inputs that have been given to the update state but were
    /// rejected, to be given back after it signals that it's ready for them.
    deferred_inputs: Vec<UpdateInput>,
}

pub fn proceed_state(
    mach: UpdateState,
    input: UpdateInput, // More inputs.
) -> Result<(UpdateState, Vec<UpdateSideEffect>)> {
    unimplemented!()
}

/// Advances the update context, given the input.  Will apply deferred inputs if
/// the state machine asks for it.  If a Finish side effect comes out then we
/// will return Ok(true).
pub fn submit_input(
    ctx: &mut UpdateContext,
    input: UpdateInput,
) -> Result<(bool, Vec<UpdateSideEffect>)> {
    use self::UpdateInput::*;
    use self::UpdateSideEffect::*;

    let mut s = ctx.update_state.clone();
    let mut fin = false;

    let mut outs = Vec::new();

    // Queue of inputs for us to process, next being popped from top.
    let mut input_queue = VecDeque::new();
    input_queue.push_back(input);

    while input_queue.len() > 0 {
        let (ns, fx) = proceed_state(s, input_queue.pop_front().unwrap())?;

        // Process some of the side effects.
        for f in fx {
            match f {
                // If this is the last step, make sure we know.
                Finish => fin = true,

                // Defer some input.
                DeferInput(i) => ctx.deferred_inputs.push(i),

                // Queue all of the deferred inputs to be processed.
                ProcessDeferred => {
                    // FIXME This could be faster, don't need to clone since we're moving it anyways.
                    input_queue.extend(ctx.deferred_inputs.iter().cloned());
                    ctx.deferred_inputs = Vec::new();
                }

                // Anything else we just add to the list to return.
                ext => outs.push(ext),
            }
        }

        // Update the expected state.
        s = ns;
    }

    // Set the state of the update context.
    ctx.update_state = s;

    Ok((fin, outs))
}

include!("state_io.rs");
