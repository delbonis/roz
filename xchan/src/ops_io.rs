impl io::BinaryRep for Op {
    fn deserialize<R: io::ReadBytesExt>(reader: &mut R) -> io::Result<Self> {
        // Write the TTL.
        let ttl = reader.read_u64::<BigEndian>()?;

        // Now write the actual data.
        let n = reader.read_u8()?;
        match n {
            0x01 => {
                // TODO Push
            }
            0x10 => {
                // TODO AddHtlc
            }
            0x11 => {
                // TODO ClaimHtlc
            }
            0x12 => {
                // TODO ExpireHtlc
            }
            0x20 => {
                // TODO FundSubchannel
                unimplemented!()
            }
            0x21 => {
                // TODO CloseSubchannel
                unimplemented!()
            }
            _ => {
                return Err(io::BinaryError::new_unexpected(format!(
                    "invalid op type {:x?}",
                    n
                )))
            }
        }

        unimplemented!()
    }
    fn serialize<W: io::WriteBytesExt>(&self, writer: &mut W) -> io::Result<()> {
        use self::OpTypeData::*;

        // Write the TTL
        writer.write_u64::<BigEndian>(self.ttl_unix)?;

        // TODO
        match &self.typedata {
            Push { amt, dest } => {}
            AddHtlc {
                size,
                timeout,
                prehash,
                dest,
                name,
            } => {}
            ClaimHtlc { preimage, name } => {}
            ExpireHtlc { name } => {}
            FundSubchannel {
                name,
                member_commitment,
                funder_withdrawals,
            } => {
                // TODO
                unimplemented!()
            }
            CloseSubchannel {
                name,
                final_state_cfg,
            } => {
                // TODO
                unimplemented!()
            }
        }

        unimplemented!()
    }
}

impl io::BinaryRep for AuthedOp {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> io::Result<Self> {
        // Read the operation, this is simple.
        let op = Op::deserialize(reader)?;

        // Now read the sig pairs, this is a little harder.
        let sp_cnt = reader.read_u16::<BigEndian>()? as usize;
        let mut pairs = Vec::new();
        for _ in 0..sp_cnt {
            // Reading the string is a little tricky.
            let addr_len = reader.read_u8()?;
            let mut addr_buf = vec![0, addr_len];
            reader.read_exact(&mut addr_buf)?;
            let addr_str = String::from_utf8(addr_buf)?;
            let addr = LnAddr::from_str(addr_str)
                .map_err(|e| io::BinaryError::new_unexpected(format!("{:?}", e)))?;

            // Read the sig, which is simpler.
            let sig = Signature::deserialize(reader)?;

            // Push it onto the vec.
            pairs.push((addr, sig))
        }

        Ok(AuthedOp {
            op: op,
            sig_pairs: pairs,
        })
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> io::Result<()> {
        // Serialize the operation first, it's the more important thing.
        self.op.serialize(writer)?;

        // Now serialize the signatures and stuff.
        writer.write_u16::<BigEndian>(self.sig_pairs.len() as u16)?;
        for (addr, sig) in self.sig_pairs.iter() {
            writer.write_u8(addr.as_string().len() as u8)?;
            writer.write(addr.as_string().as_ref())?;
            sig.serialize(writer)?;
        }

        // Ok.
        Ok(())
    }
}

impl io::BinaryRep for StateTransition {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> io::Result<Self> {
        // Read the timestamp.
        let ts = reader.read_u64::<BigEndian>()?;

        // Read the ops, annoying.
        let op_cnt = reader.read_u16::<BigEndian>()?;
        let mut ops = Vec::new();
        for _ in 0..op_cnt {
            ops.push(AuthedOp::deserialize(reader)?);
        }

        // Return
        Ok(StateTransition {
            timestamp: ts,
            ops: ops,
        })
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> io::Result<()> {
        // Write the timestamp, simple.
        writer.write_u64::<BigEndian>(self.timestamp)?;

        // Write the ops, a little more annoying.
        writer.write_u16::<BigEndian>(self.ops.len() as u16)?;
        for ao in &self.ops {
            ao.serialize(writer)?;
        }

        // Ok.
        Ok(())
    }
}
