use std::fmt;
use std::hash::{Hash, Hasher};

use crypto::digest::Digest;
use crypto::sha2;
use secp256k1;

use io;

#[derive(Clone, Debug)]
pub struct Keypair {
    sk: secp256k1::SecretKey,
    pk: secp256k1::PublicKey,
}

impl Keypair {
    pub fn sign_msg(&self, msg: &[u8]) -> Signature {
        let curve = secp256k1::Secp256k1::signing_only();
        let hash = {
            let mut h = sha2::Sha256::new();
            h.input(msg);
            let mut buf = [0; 32];
            h.result(&mut buf);
            buf
        };

        let m = secp256k1::Message::from_slice(&hash).unwrap();
        Signature::from_native(curve.sign(&m, &self.sk))
    }

    pub fn to_pubkey(&self) -> Pubkey {
        Pubkey {
            pk: self.pk.clone(),
        }
    }

    pub fn verify_msg(&self, sig: Signature, msg: &[u8]) -> bool {
        self.to_pubkey().verify_msg(sig, msg) // TODO reduce cloning
    }

    pub fn verify_hash(&self, sig: Signature, hash: &[u8]) -> bool {
        self.to_pubkey().verify_hash(sig, hash) // TODO reduce cloning
    }
}

impl Into<Pubkey> for Keypair {
    fn into(self) -> Pubkey {
        Pubkey { pk: self.pk }
    }
}

impl Hash for Keypair {
    fn hash<H: Hasher>(&self, state: &mut H) {
        state.write("private key".as_bytes()); // FIXME This isn't 100% perfect but should work ok enough.
        state.write(&self.pk.serialize());
    }
}

#[derive(Clone, Eq, PartialEq, Debug)]
pub struct Pubkey {
    pk: secp256k1::PublicKey,
}

impl Pubkey {
    pub fn verify_msg(&self, sig: Signature, msg: &[u8]) -> bool {
        let hash = {
            let mut h = sha2::Sha256::new();
            h.input(msg);
            let mut buf = [0; 32];
            h.result(&mut buf);
            buf
        };
        self.verify_hash(sig, &hash)
    }

    pub fn verify_hash(&self, sig: Signature, hash: &[u8]) -> bool {
        let curve = secp256k1::Secp256k1::verification_only();
        let m = secp256k1::Message::from_slice(hash);
        let nsig = sig.into_native();
        if m.is_err() || nsig.is_err() {
            return false; // TODO Make this better.
        }

        curve.verify(&m.unwrap(), &nsig.unwrap(), &self.pk).is_ok()
    }
}

impl Hash for Pubkey {
    fn hash<H: Hasher>(&self, state: &mut H) {
        use io::BinaryRep;
        let mut buf = Vec::new();
        self.serialize(&mut buf).unwrap();
        state.write(buf.as_ref());
    }
}

/// This is a secp256k1 signature in compressed form.
#[derive(Clone)]
pub struct Signature([u8; 64]);

impl Signature {
    pub fn new(sig: [u8; 64]) -> Signature {
        Signature(sig)
    }

    fn from_native(nsig: secp256k1::Signature) -> Signature {
        Signature(nsig.serialize_compact(&secp256k1::Secp256k1::without_caps()))
    }

    fn into_native(self) -> Result<secp256k1::Signature, secp256k1::Error> {
        secp256k1::Signature::from_compact(&secp256k1::Secp256k1::without_caps(), &self.0)
    }
}

impl io::BinaryRep for Pubkey {
    fn deserialize<R: io::ReadBytesExt>(reader: &mut R) -> io::Result<Self> {
        let mut buf = [0; 33];
        reader.read_exact(&mut buf)?;
        let curve = secp256k1::Secp256k1::without_caps();
        let deser = secp256k1::PublicKey::from_slice(&curve, &buf)
            .map_err(|_| io::BinaryError::new_unexpected("couldn't decode pubkey point".into()))?;
        Ok(Pubkey { pk: deser })
    }
    fn serialize<W: io::WriteBytesExt>(&self, writer: &mut W) -> io::Result<()> {
        let buf = self.pk.serialize();
        writer.write_all(&buf)?;
        Ok(())
    }
}

impl io::BinaryRep for Signature {
    fn deserialize<R: io::ReadBytesExt>(reader: &mut R) -> io::Result<Self> {
        let mut buf = [0; 64];
        reader.read_exact(&mut buf)?;
        Ok(Signature(buf))
    }
    fn serialize<W: io::WriteBytesExt>(&self, writer: &mut W) -> io::Result<()> {
        writer.write_all(&self.0)?;
        Ok(())
    }
}

impl Hash for Signature {
    fn hash<H: Hasher>(&self, state: &mut H) {
        state.write(&self.0); // straightforward enough
    }
}

impl fmt::Debug for Signature {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(format!("{:?}", &self.0[..]).as_ref())
    }
}

impl PartialEq for Signature {
    fn eq(&self, o: &Self) -> bool {
        for i in 0..self.0.len() {
            if self.0[i] != o.0[i] {
                return false;
            }
        }
        true
    }
}

impl Eq for Signature {}
