#![allow(unused)]

use std::rc::Rc;

use crypto::digest::Digest;
use crypto::sha2::Sha256;

use io::{self, BinaryError, BinaryRep, ReadBytesExt, WriteBytesExt};

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
pub struct MerkleRoot([u8; 32]);

#[derive(Copy, Clone, Hash, Debug)]
enum Side {
    Left,
    Right,
}

#[derive(Clone, Hash, Debug)]
pub struct MerkleBranch {
    parent: Option<Rc<MerkleBranch>>,
    cohash: [u8; 32],
    coside: Side,
}

#[derive(Clone, Hash, Debug)]
pub enum MerkleProof {
    Trivial,
    Branch(Rc<MerkleBranch>),
}

pub fn compute_merkle_proofs<T: BinaryRep>(items: &Vec<T>) -> (MerkleRoot, Vec<MerkleProof>) {
    // First just hash everything.
    let mut base_hashes: Vec<[u8; 32]> = items.iter().map(|i| hash_item(i)).collect();

    // If there's only 1 hash, just return it as the root and don't provide any proofs.
    if base_hashes.len() == 1 {
        return (MerkleRoot(base_hashes[0]), vec![MerkleProof::Trivial]);
    }

    // Make sure we have a power of 2 hashes and that there's >1.
    while base_hashes.len().count_ones() != 1 && base_hashes.len() >= 2 {
        base_hashes.push([0; 32]);
    }

    // Generate the merkle root, this is simple.
    let root = gen_hash_merkle_root(base_hashes.as_slice());

    // Generate all the merkle proofs, this is a little harder, since we have to
    // wrap up the branches into opaque MerkleProofs.
    let proofs = gen_hash_merkle_proofs(base_hashes.as_slice())
        .into_iter()
        .map(MerkleProof::Branch)
        .collect();

    (root, proofs)
}

/// Generates proofs for all of the hashes in the passed slice.  Should be a
/// power of 2 in length, but it doesn't check.  Will break in spooky ways if
/// you don't pass it something of the right length.
fn gen_hash_merkle_proofs(hashes: &[[u8; 32]]) -> Vec<Rc<MerkleBranch>> {
    if hashes.len() == 2 {
        // Actually generate the base proofs.
        let left = MerkleBranch {
            parent: None,
            cohash: hashes[1].clone(),
            coside: Side::Right,
        };
        let right = MerkleBranch {
            parent: None,
            cohash: hashes[0].clone(),
            coside: Side::Left,
        };

        // Return
        vec![Rc::new(left), Rc::new(right)]
    } else {
        let mut proofs = Vec::new();

        // Split up the hashes and match things up with the subtrees.
        let mut pair_hashes = Vec::new();
        for i in 0..hashes.len() / 2 {
            pair_hashes.push(combine_branches(&hashes[i * 2], &hashes[i * 2 + 1]));
        }

        // Now get the proofs for the parent level.
        let parent_proofs = gen_hash_merkle_proofs(pair_hashes.as_slice());

        for i in 0..hashes.len() {
            // Figure out the info we need for the proofs.
            let p = parent_proofs[i / 2].clone();
            let ch = hashes[i - (1 - i % 2)]; // gets the sibling leaf
            let cs = if i % 2 == 0 { Side::Right } else { Side::Left };

            // Put it together.
            proofs.push(Rc::new(MerkleBranch {
                parent: Some(p),
                cohash: ch.clone(),
                coside: cs,
            }));
        }

        // Return
        proofs
    }
}

/// Figures out the merkle root of the hashes, same disclaimer as above.
fn gen_hash_merkle_root(hashes: &[[u8; 32]]) -> MerkleRoot {
    if hashes.len() == 1 {
        MerkleRoot(hashes[0].clone())
    } else {
        let middle = hashes.len() / 2;
        let l = gen_hash_merkle_root(&hashes[..middle]);
        let r = gen_hash_merkle_root(&hashes[middle..]);
        MerkleRoot(combine_branches(&l.0, &r.0))
    }
}

fn combine_branches(l: &[u8; 32], r: &[u8; 32]) -> [u8; 32] {
    let mut sha = Sha256::new();
    sha.input(l);
    sha.input(r);
    let mut out = [0; 32];
    sha.result(&mut out);
    out
}

pub fn verify_proof<T: BinaryRep>(item: &T, proof: MerkleProof, root: MerkleRoot) -> bool {
    // Hash the item we're checking membership of.
    let mut cur_hash = hash_item(item);

    // Figure out where the base branch is, or if we can just quick escape.
    let mut cur_branch = match proof {
        MerkleProof::Trivial => return cur_hash == root.0,
        MerkleProof::Branch(b) => Some(b),
    };

    // Loop until we're done with the zigzag thing.
    while !cur_branch.is_none() {
        let b = cur_branch.unwrap();
        match b.coside {
            Side::Left => {
                cur_hash = combine_branches(&b.cohash, &cur_hash);
            }
            Side::Right => {
                cur_hash = combine_branches(&cur_hash, &b.cohash);
            }
        }
        cur_branch = b.parent.clone();
    }

    // Then make sure where we ended up is actually the root.
    cur_hash == root.0
}

/// Verifies that the items provided generate the particular root.
pub fn verify_root<T: BinaryRep>(items: &Vec<T>, root: MerkleRoot) -> bool {
    let (croot, _) = compute_merkle_proofs(items); // does too much, fix later
    root == croot // EZ
}

fn hash_item<T: BinaryRep>(item: &T) -> [u8; 32] {
    // Serialize
    let mut buf = Vec::new();
    item.serialize(&mut buf);

    // Hash
    let mut sha = Sha256::new();
    sha.input(&buf.as_slice());
    let mut out = [0; 32];
    sha.result(&mut out);

    // Return
    out
}

impl BinaryRep for MerkleRoot {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> io::Result<Self> {
        let mut buf = [0; 32];
        reader.read_exact(&mut buf)?;
        Ok(MerkleRoot(buf))
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> io::Result<()> {
        Ok(writer.write_all(&self.0)?)
    }
}

impl BinaryRep for MerkleProof {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> io::Result<Self> {
        // If there's more than 256 steps then something is *wrong*.
        let height = reader.read_u8()?;
        if height == 0 {
            return Ok(MerkleProof::Trivial); // early return for trivial proofs
        }

        // Now keep eating proofs until you get there.
        let mut cur_node = None;
        for _ in 0..height {
            // Read the co-side.
            let cs = match reader.read_u8()? {
                0 => Side::Left,
                1 => Side::Right,
                n => {
                    return Err(io::BinaryError::new_unexpected(format!(
                        "bad side num {}",
                        n
                    )))
                }
            };

            // Read the cohash.
            let mut ch = [0; 32];
            reader.read_exact(&mut ch)?;

            // Update the tip.
            cur_node = Some(Rc::new(MerkleBranch {
                parent: cur_node.clone(),
                cohash: ch,
                coside: cs,
            }))
        }

        Ok(MerkleProof::Branch(cur_node.unwrap()))
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> io::Result<()> {
        match &self {
            &MerkleProof::Trivial => {
                writer.write_u8(0x00)?;
            }
            &MerkleProof::Branch(b) => {
                // Push all the branches onto a stack we can traverse in reverse.
                let mut branches = Vec::new();
                branches.push(b.clone());
                let mut cur = b;
                while let Some(ref p) = cur.parent {
                    branches.push(p.clone());
                    cur = p;
                }

                // Sanity check.
                if branches.len() > 255 {
                    return Err(io::BinaryError::new_unexpected(format!(
                        "merkle proof path too long: {}",
                        branches.len()
                    )));
                }

                // Now write the actual data.
                writer.write_u8(branches.len() as u8)?;
                for b in branches {
                    writer.write_u8(match b.coside {
                        Side::Left => 0,
                        Side::Right => 1,
                    })?;
                    writer.write_all(&b.cohash)?;
                }
            }
        }
        Ok(())
    }
}
