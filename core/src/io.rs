#![allow(unused)]

use std::convert;
use std::io;
use std::mem;
use std::string;

pub use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};

/// For errors that can occur when dealing with BinaryRep.
#[derive(Debug)]
pub enum BinaryError {
    Io(io::Error),
    Utf8Format(string::FromUtf8Error),
    Overflow,
    UnexpectedValue(String),
}

impl BinaryError {
    pub fn new_unexpected(msg: String) -> BinaryError {
        BinaryError::UnexpectedValue(msg)
    }
}

impl From<io::Error> for BinaryError {
    fn from(f: io::Error) -> Self {
        BinaryError::Io(f)
    }
}

impl From<string::FromUtf8Error> for BinaryError {
    fn from(f: string::FromUtf8Error) -> Self {
        BinaryError::Utf8Format(f)
    }
}

/// Result type used in this module.
pub type Result<T> = ::std::result::Result<T, BinaryError>;

/// This trait defines the mapping between a type and its binary representation.
pub trait BinaryRep: Clone + Sized {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> Result<Self>;
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> Result<()>;
}

impl BinaryRep for u8 {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> Result<Self> {
        Ok(reader.read_u8()?)
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> Result<()> {
        Ok(writer.write_u8(*self)?)
    }
}

impl BinaryRep for u16 {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> Result<Self> {
        Ok(reader.read_u16::<BigEndian>()?)
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> Result<()> {
        Ok(writer.write_u16::<BigEndian>(*self)?)
    }
}

impl BinaryRep for u32 {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> Result<Self> {
        Ok(reader.read_u32::<BigEndian>()?)
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> Result<()> {
        Ok(writer.write_u32::<BigEndian>(*self)?)
    }
}

impl BinaryRep for u64 {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> Result<Self> {
        Ok(reader.read_u64::<BigEndian>()?)
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> Result<()> {
        Ok(writer.write_u64::<BigEndian>(*self)?)
    }
}

impl BinaryRep for i32 {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> Result<Self> {
        Ok(reader.read_i32::<BigEndian>()?)
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> Result<()> {
        Ok(writer.write_i32::<BigEndian>(*self)?)
    }
}

impl<T: BinaryRep> BinaryRep for [T; 32] {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> Result<Self> {
        let mut buf: [T; 32] = unsafe { mem::uninitialized() };
        for i in 0..32 {
            buf[i] = T::deserialize(reader)?;
        }
        Ok(buf)
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> Result<()> {
        for v in self {
            v.serialize(writer)?;
        }
        Ok(())
    }
}

impl<T: BinaryRep> BinaryRep for Option<T> {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> Result<Self> {
        match reader.read_u8()? {
            0 => Ok(None),
            1 => Ok(Some(T::deserialize(reader)?)),
            n => Err(BinaryError::new_unexpected(format!(
                "option invalid variant {}",
                n
            ))),
        }
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> Result<()> {
        match self {
            Some(v) => {
                writer.write_u8(0x01)?;
                v.serialize(writer)?;
            }
            None => writer.write_u8(0x00)?,
        }
        Ok(())
    }
}

impl<T, E> BinaryRep for ::std::result::Result<T, E>
where
    T: BinaryRep,
    E: BinaryRep,
{
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> Result<Self> {
        match reader.read_u8()? {
            0 => Ok(Ok(T::deserialize(reader)?)),
            1 => Ok(Err(E::deserialize(reader)?)),
            n => Err(BinaryError::new_unexpected(format!(
                "result invalid variant {}",
                n
            ))),
        }
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> Result<()> {
        match self {
            Ok(v) => {
                writer.write_u8(0x00)?;
                v.serialize(writer)?;
            }
            Err(e) => {
                writer.write_u8(0x01)?;
                e.serialize(writer)?;
            }
        }
        Ok(())
    }
}
