#![allow(unused)]

use io::*;

use std::convert::{TryFrom, TryInto};

/// A blob is a simpler container type for some inner data that handles serialization
/// automatically.  It's undefined to store more than the max length that can be represented by
/// the wrapping type in the blob, so do that yourself.
#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub struct Blob<L>(Vec<u8>, ::std::marker::PhantomData<L>); // ignore this spoopy phantomdata

/// A small blob can hold up to 255 bytes of data.
pub type SmallBlob = Blob<u8>;

/// A medium blob can hold up to 65535 bytes of data, or ~64 KiB.
pub type MediumBlob = Blob<u16>;

/// A large blob can hold up to 2^32 - 1 bytes of data, or ~4 GiB.
pub type LargeBlob = Blob<u32>;

impl<L: BinaryRep + TryInto<usize> + TryFrom<usize>> BinaryRep for Blob<L> {
    fn deserialize<R: ReadBytesExt>(reader: &mut R) -> Result<Self> {
        let len = L::deserialize(reader)?
            .try_into()
            .map_err(|_| BinaryError::new_unexpected("could not convert value".into()))?;

        let mut buf = vec![0; len];
        reader.read_exact(&mut buf)?;
        Ok(Blob(buf, ::std::marker::PhantomData))
    }
    fn serialize<W: WriteBytesExt>(&self, writer: &mut W) -> Result<()> {
        let len: L = (self.0.len())
            .try_into()
            .map_err(|_| BinaryError::new_unexpected("could not convert length".into()))?;

        BinaryRep::serialize(&len, writer)?; // not the "normal" way to do this, but looks better here
        writer.write_all(self.0.as_slice())?;
        Ok(())
    }
}

impl<L> Blob<L> {
    /// Wraps the provided vec as a blob, without checking size constraints.
    pub fn new_unchecked(v: Vec<u8>) -> Blob<L> {
        Blob(v, ::std::marker::PhantomData)
    }

    /// Makes a copy of the slice as a blob, without checking size constraints.
    pub fn from_slice_unchecked(v: &[u8]) -> Blob<L> {
        Self::new_unchecked(Vec::from(v))
    }

    /// Attempts to convert the blob into a string.
    pub fn try_into_string(self) -> Result<String> {
        Ok(String::from_utf8(self.0)?)
    }

    /// Returns the contents as a slice of u8s.
    pub fn as_slice(&self) -> &[u8] {
        self.0.as_slice()
    }

    /// Returns a ref to the inner vec.
    pub fn inner(&self) -> &Vec<u8> {
        &self.0
    }

    /// Unwraps self into the inner vec.
    pub fn into_inner(self) -> Vec<u8> {
        self.0
    }

    /// Returns the contents of self as a vec.
    pub fn len(&self) -> usize {
        self.0.len()
    }
}

/// This is a type with some utils for ensuring that blobs are the right sizes for their contents.
#[derive(Clone, Debug)]
pub enum SizedBlob {
    Small(SmallBlob),
    Medium(MediumBlob),
    Large(LargeBlob),
}

impl SizedBlob {
    /// Creates a new blob of the minimum size that will support the data.
    pub fn new_min(data: Vec<u8>) -> Result<SizedBlob> {
        use std::{u16, u32, u8};
        Ok(match data.len() {
            // pls ignore the phantomdata stuff again here
            n if n <= u8::MAX as usize => {
                SizedBlob::Small(Blob::<u8>(data, ::std::marker::PhantomData))
            }
            n if n <= u16::MAX as usize => {
                SizedBlob::Medium(Blob::<u16>(data, ::std::marker::PhantomData))
            }
            n if n <= u32::MAX as usize => {
                SizedBlob::Large(Blob::<u32>(data, ::std::marker::PhantomData))
            }
            _ => return Err(BinaryError::Overflow),
        })
    }

    pub fn new_u8_blob(data: Vec<u8>) -> Result<SmallBlob> {
        use std::u8;
        if data.len() <= u8::MAX as usize {
            Ok(Blob::<u8>(data, ::std::marker::PhantomData))
        } else {
            Err(BinaryError::Overflow)
        }
    }
}
