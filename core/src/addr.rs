
use bech32;

#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub struct LnAddr(String);

#[derive(Debug)]
pub enum Error {
    Bech32(bech32::Error),
    BadPrefix,
    PkhTooLong
}

impl LnAddr {

    pub fn from_str(m: String) -> Result<LnAddr, Error> {

        let b = bech32::Bech32::from_str_lenient(m.as_ref()).map_err(|e| Error::Bech32(e))?;

        // Check the prefix.
        if b.hrp() != "ln" {
            return Err(Error::BadPrefix);
        }

        // Make sure the pubkey hash isn't too long, it should be 20 or less.
        if bech32::convert_bits(b.data(), 5, 8, true).unwrap().len() <= 20 {
            return Err(Error::PkhTooLong);
        }

        Ok(LnAddr(m))

    }

    pub fn as_string(&self) -> &String {
        &self.0
    }

    pub fn to_string(&self) -> String {
        self.0.clone()
    }

    pub fn to_pkh(&self) -> Vec<u8> {
        // these *probably* won't panic, I think
        let b = bech32::Bech32::from_str_lenient(self.0.as_ref()).unwrap();
        bech32::convert_bits(b.data(), 5, 8, true).unwrap()
    }

}
