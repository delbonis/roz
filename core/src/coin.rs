#![allow(unused)]

use std::fmt;
use std::io;

use std::sync::*;

/// Defines data about a coin.
#[derive(Clone)]
pub struct Coin {
    /// The human-readable name of the coin, like "Bitcoin" or "Litcoin".
    name: String,

    /// The slug form of the name, like "bitcoin" or "bitcoin-testnet".
    slug: String,

    /// The kind of coin it is, see the CoinNet enum.
    network: CoinNet,

    /// The family of coins this coin is of, see the CoinFamily enum.
    family: CoinFamily,

    /// Returns if the str passed is a valid format for this coin's addresses.
    parse_address: ParseAddrFn,

    /// Returns if the slice passed seems to be a valid public key we could directly pay to, probably true.
    parse_pubkey: ParseChainPubkeyFn,

    /// Returns the blob that should be signed by the privkey in order to produce a sig for a tx.
    generate_tx_blob: GenTxBlobFn,
}

/// A coin ID is something that represents the coin, usually the chainhash.
#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub struct CoinId(Box<[u8]>);

impl fmt::Debug for Coin {
    fn fmt(&self, f: &mut fmt::Formatter) -> ::std::result::Result<(), fmt::Error> {
        f.write_str(self.slug.as_ref())
    }
}

#[derive(Debug)]
pub enum CoinError {
    InvalidFormat(String),
}

macro_rules! coin_fn {
    ( $( $arg:ty ),* => $ret:ty ) => { Arc<Fn( $( $arg, )* ) -> $ret + Sync + Send> };
    ( $ret:ty ) => { Arc<Fn() -> $ret + Sync + Send> };
}

pub type Result<T> = ::std::result::Result<T, CoinError>;

// TODO Make this not arcs.
pub type ParseAddrFn = coin_fn!(&str => Result<ChainAddress>);
pub type ParseChainPubkeyFn = coin_fn!(&[u8] => Result<ChainPubkey>);
pub type GenTxBlobFn = coin_fn!(TransactionPlan => Vec<u8>);

#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub struct Txid(Box<[u8]>);

#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub struct TxIn(Box<[u8]>); // TODO Needs to expose more details.

#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub struct TxOut(Box<[u8]>);

#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub struct ChainPubkey(Box<[u8]>);

impl ChainPubkey {
    pub fn from_slice(v: &[u8]) -> ChainPubkey {
        ChainPubkey(Vec::from(v).into_boxed_slice())
    }

    pub fn from_vec(v: Vec<u8>) -> ChainPubkey {
        ChainPubkey(v.into_boxed_slice())
    }

    pub fn as_slice(&self) -> &[u8] {
        self.0.as_ref()
    }
}

#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub struct ChainAddress(Box<[u8]>);

#[derive(Clone, Eq, PartialEq, Debug)]
pub enum CoinNet {
    Mainnet,
    Testnet,
    Regtest,
}

#[derive(Clone, Eq, PartialEq, Debug)]
pub enum CoinFamily {
    Satoshi,
}

/// A high-level definition of a contract to be rendered by the tx builder.
/// TODO This needs to be fleshed out a bit more.
#[derive(Clone, Hash, Debug)]
pub enum Contract {
    /// P2(W)PKH
    Pubkey(ChainPubkey),

    /// P2(W)SH, .1-of-.0.len() multisig
    Multisig(Vec<ChainPubkey>, usize),

    /// HTLC contract as in Lightning
    Htlc {
        dest: ChainAddress,
        refund: ChainAddress,
        prehash: Box<[u8]>,
        timeout: u32,
    },

    /// Just a raw tx blob, you shouldn't use this unless you know what you're
    /// doing since its behavior is implementation-defined.  (Raw script in Bitcoin.)
    RawUnsafe(Box<[u8]>),
}

#[derive(Clone)]
pub struct TransactionPlan {
    txins: Vec<TxIn>,
    txouts: Vec<TxOut>,
    // nLockTime?
}

impl TransactionPlan {
    pub fn new(txins: Vec<TxIn>, txouts: Vec<TxOut>) -> TransactionPlan {
        TransactionPlan { txins, txouts }
    }
}
