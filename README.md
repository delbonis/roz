# Roz

`"lit".chars().map(|c| c + 'r' - 'l').collect::<String>()`

Experimental lightning network implementation and associated libraries, aiming
to be network-compatible with [Lit](https://github.com/mit-dci/lit).
